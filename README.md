## DStabilize

DStabilize offers three resources. 

1. Conversion between L- and D-forms of proteins.
2. Applying a custom transformation matrix to the protein structure.
3. Generate retro-inverso forms of peptides.

The text below explains the user interface.

### Main Page

The [main page](http://dstabilize.bii.a-star.edu.sg/) presents the user with all of the above listed resources. 
Clicking an appripriate button loads the interface for each resource. 

#### Generate D-form structures

The interface allows the user to either upload a structure or request one from [RCSB](http://rcsb.org). The structural data provided is reflected in the _yz_-plane. The result page shows two [NGL](https://github.com/arose/ngl) viewer windows, one showing the protein structure provided to the program, _Original PDB_, and the other showing the transformed data, _Transformed PDB_. The windows have limited interactabilty allowing users to quickly see changes made to the protein structures.The accompanying link at the bottom provides a zipped file containing the two PDBs.  

	
#### Generate Custom transformed structures

As before the interface allows users to either upload a structure or request one from [RCSB](http://rcsb.org). The custom transformation matrix is provided using the matrix table shown. The first three rows and first three columns hold values of the rotation around one of the coordinate axes. The fourth column holds values by which the structure is translated in the _x_, _y_ and _z_ directions. Please see the 3D transformation section of this [wiki](https://en.wikipedia.org/wiki/Transformation_matrix#Examples_in_3D_computer_graphics) for more information on how to populate the 4x4 transformation matrices. 

#### Generate retro-inverso form of peptides
	
In this case the user has to upload a structure and the starting and ending residue numbers. The retro-inverso form will be generated for residues between this range of residues.
	
For single chain structures, the first residue can have any value. Starting and ending residues, as needed above, should match the numbering in the PDB file as is.
	
This programs employs _tleap_ from [ambertools](https://ambermd.org/AmberTools.php). As a consequence, multichain protein assemblies get continuous residue numbering. For instance with a dimeric assembly i.e. Chain A and B, having residues labelled as 1-100 and 1-10 each, will become numbered as 1-110. As a result, if the retro-inverso form of chain B is required, the user has to enter 101 as the starting residue and 110 as the final residue. Therefore, for multichain assemblies the user should run _tleap_ locally and upload the output with starting and ending residues corresponding to this new structure. 
	
	
### Disclaimer: 

**D-form structure generation and applying custom transformations**
	
1. The program cannot distinguish if the uploaded structure is _L_-form or _D_-form. The program only converts between the two. If a _L_-form structure is provided, the output would be _D_-form and vice versa.  
2. The rotation is applied about the origin.  
3. The above two resources make the assumption that the input structure follows standard PDB formatting. In reality there is no restriction on the content of the input data (i.e. protein, DNA, RNA etc) as long as the PDB [atom coordinate formatting](https://www.wwpdb.org/documentation/file-format-content/format33/sect9.html#ATOM) is followed.
	
**Retro-Inverso structure generation**
	
1. The program does not check for missing residues in the structure. If residues are missing, the numbering will be offset and might impact the output.
2. The program currently anticipates only standard amino acids. It is therefore recommended to always check the output file.
3. The program does not optimise the output. It is likely that the output might have steric clashes, especially in cases where prolines are involved. Minimization is therefore recommended for geometry optimization.


### Found a bug: 

For queries related to D-form structure generation and applying custom transformation please contact: [Ashar Malik](mailto:asharjm@bii.a-star.edu.sg)

For queries related to Retro-inverso structure generation please contact: [Pietro Aronica](mailto:pietroa@bii.a-star.edu.sg)